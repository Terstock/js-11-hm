/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?
Подія - це сигнал того, що щось сталося.
2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.

mousedown/mouseup,mouseover/mouseout, mousemove, click,contextmenu
3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
contextmenu - це коли користувач правою кнопкою миші клікає на елемент.

Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
  */

let butn = document.querySelector("#btn-click");
let sect = document.querySelector("#content");

butn.addEventListener("click", function () {
  let par = document.createElement("p");
  par.textContent = "New Paragraph";
  sect.prepend(par);
});

// console.log(butn);

//  2. Додати новий елемент форми із атрибутами:
//  Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
//   По кліку на створену кнопку, створіть новий елемент <input> і додайте до
//   нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.

let buttonNew = document.createElement("button");
buttonNew.setAttribute("id", "btn-input-create");
buttonNew.setAttribute("class", "btn-input-create");
buttonNew.textContent = "Create input";
sect.append(buttonNew);

buttonNew.addEventListener("click", function () {
  let input = document.createElement("input");
  input.setAttribute("type", "text");
  input.setAttribute("placeholder", "Enter:");
  input.setAttribute("name", "inputus");
  sect.append(input);
});

console.log(buttonNew);
